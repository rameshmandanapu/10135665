package ho.qat.seo.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

public class FamilymemberstatusPage extends PageObject {

    @FindBy(css = "#current-question > button")
    private WebElementFacade nextStepButton;

    @FindBy(xpath = "//div[@class=\"gem-c-radio govuk-radios__item\"]/input[@value='yes']")
    WebElementFacade selectyesornoradios;

    public void selectimmegrationstatus(String reason){
        String selector = String.join("","input[value='", reason.toLowerCase(), "']");
        selectyesornoradios = find(By.cssSelector(selector));
        selectyesornoradios.waitUntilEnabled().click();
    }

    public void clickNextStepButton(){

        nextStepButton.waitUntilEnabled().click();
    }

}
