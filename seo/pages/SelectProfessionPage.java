package ho.qat.seo.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

public class SelectProfessionPage extends PageObject {

    @FindBy(css = "#current-question > button")
    private WebElementFacade nextStepButton;

    @FindBy(xpath = "//div[@class=\"govuk-form-group govuk-!-margin-bottom-6\"]")
    WebElementFacade selectprofessionfromlist;

    public void selectProfession(String reason){
        String selector = String.join("","input[value='", reason.toLowerCase(), "']");
        selectprofessionfromlist = find(By.cssSelector(selector));
        selectprofessionfromlist.waitUntilEnabled().click();
    }

    public void clickNextStepButton(){

        nextStepButton.waitUntilEnabled().click();
    }

}
