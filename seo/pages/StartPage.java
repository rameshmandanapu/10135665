package ho.qat.seo.pages;

import net.serenitybdd.annotations.DefaultUrl;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.gov.uk/check-uk-visa")
public class StartPage extends PageObject {

        @FindBy(css = "#content > div:nth-child(2) > div.govuk-grid-column-two-thirds > article > section.govuk-\\!-margin-bottom-6 > a")
        private WebElementFacade startNowButton;

        public void clickStartNow(){
            clickOn(startNowButton);
        }

}
