package ho.qat.seo.pages;

import net.serenitybdd.core.pages.PageObject;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RestAssuredAPItest extends PageObject {
    @Test
    public void main() {
        // Replace 'YOUR_API_KEY' with the actual API key you obtained
        String apiKey = "7f7e07c1-aa70-471a-873b-9d19785b1462";
//        http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/sitelist?key==7f7e07c1-aa70-471a-873b-9d19785b1462

//                val/wxobs/all/xml/324152?res=hourly&key
        // Set up the base URL for the Met Office DataPoint API
        String baseUrl = "http://datapoint.metoffice.gov.uk/public/data/";

        try {
            // Step 1: Get the Location ID for 'Croydon'
            String sitelistUrl = baseUrl + "val/wxfcs/all/json/sitelist?key=" + apiKey;
            String sitelistResponse = sendHttpRequest(sitelistUrl);
            String croydonLocationId = getLocationId(sitelistResponse, "Croydon");

            if (croydonLocationId == null) {
                System.out.println("Croydon location not found.");
            } else {
                System.out.println("Croydon Location ID: " + croydonLocationId);

                // Step 2: Get the daily forecast information for Croydon
                String dailyForecastUrl = baseUrl + "val/wxobs/all/xml/" + croydonLocationId + "?res=hourly&key=" + apiKey;
                String dailyForecastResponse = sendHttpRequest(dailyForecastUrl);

                // Step 3: Assert that the correct location is accessed
                assertLocationId(dailyForecastResponse, croydonLocationId);

//                 Step 4: Assert that the parameter with name 'S' has a description of wind speed
                assertWindSpeedDescription(dailyForecastResponse);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String sendHttpRequest(String url) throws IOException {
        URL apiUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) apiUrl.openConnection();
        connection.setRequestMethod("GET");

        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder response = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {
            response.append(line);
        }

        reader.close();
        connection.disconnect();

        return response.toString();
    }

    public static String getLocationId(String sitelistResponse, String locationName) {
        // Implement logic to extract Location ID from sitelistResponse
        // For simplicity, we use a basic string search here
        if (sitelistResponse.contains("\"name\":\"" + locationName + "\"")) {
            int startIndex = sitelistResponse.indexOf("\"id\":\"") + 6;
            int endIndex = sitelistResponse.indexOf("\"", startIndex);
            return sitelistResponse.substring(startIndex, endIndex);
        }

        return null;
    }

    public static void assertLocationId(String dailyForecastResponse, String expectedLocationId) {
        // Implement logic to assert the correct location ID from dailyForecastResponse
        // For simplicity, we use a basic string search here
        assert dailyForecastResponse.contains("\"i\":\"" + expectedLocationId + "\"") :
                "Incorrect location ID in the daily forecast response.";
    }

    private static void assertWindSpeedDescription(String dailyForecastResponse) {
//     For simplicity, we use a basic string search here
        assert dailyForecastResponse.contains("\"name\":\"S\",\"$\":\"Wind Speed\"") :
                "Wind speed description not found in the daily forecast response.";
    }
}