package ho.qat.seo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Croydonlocation {

    public static void main(String[] args) {
        // Set the path to your ChromeDriver executable
        System.setProperty("webdriver.chrome.driver", "src/test/resources/webDrivers/mac/chromedriver");

        // Initialize ChromeDriver
        WebDriver driver = new ChromeDriver();

        // Open the webpage
        driver.get("http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/sitelist?key=7f7e07c1-aa70-471a-873b-9d19785b1462");  // Replace with the actual URL

        // Replace this with your actual text containing word and ID information
        String text = "elevation\":\"55.0\",\"id\":\"324152\",\"latitude\":\"51.3775\",\"longitude\":\"-0.0933\",\"name\":\"Croydon\",\"region\":\"se\",\"unitaryAuthArea\":\"Greater London";

        // Replace this with the word you want to search for
        String wordToSearch = "Croydon";

        // Construct the regular expression pattern
        String patternString = "Word: " + Pattern.quote(wordToSearch) + " ID: (\\d+)";
        Pattern pattern = Pattern.compile(patternString);

        // Create a Matcher object
        Matcher matcher = pattern.matcher(text);

        // Perform the search
        if (matcher.find()) {
            // Print the ID associated with the word
            String id = matcher.group(1);
            System.out.println("Word '" + wordToSearch + "' found with ID: " + id);
        } else {
            System.out.println("Word '" + wordToSearch + "' not found in the text.");
        }
    }
}